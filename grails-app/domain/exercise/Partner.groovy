package exercise

class Partner {

    String companyName
    String ref
    Locale locale
    // Using Date is a requirement of the exercise, I would prefer:
//    LocalDateTime expires
    Date expires

    static constraints = {
        ref unique: true
    }
}
